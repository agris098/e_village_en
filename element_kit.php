<?php require '_global.php';?>
<!doctype html>
<html lang="en">
  	<head>
		<title>Element kit</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<!--END CSS -->
		<!--START JS -->
		<script src="/js/jquery-3.4.1.min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->
	</head>
	<body>
		<?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
		<?php include 'shared/_termometerWindowForm.php';?>
		<div class="container-fluid my-5">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<h2 class="mb-5">Element Kit</h2>
						</div>
						<div class="col-lg-8">
							<p>
								<i>Lorem ipsum dolor sit amet</i>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</p>
							<p>
								<b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, <u>sunt in culpa qui officia deserunt mollit anim id est laborum.</u>								
							</p>
							<ul class="e-list pl-3">
								<li>List element 1</li>
								<li>List element 2</li>
								<li>List element 3</li>
							</ul>
							<ul class="e-list">
								<li>List element 1</li>
								<li>List element 2</li>
								<li>List element 3</li>
							</ul>
							<p>
								<b>Lorem ipsum dolor sit amet, <u>consectetur</u></b> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, <u>sunt in culpa qui officia deserunt mollit anim id est laborum.</u>								
							</p>
							<ol class="e-list pl-3">
								<li>List element 1</li>
								<li>List element 2</li>
								<li>List element 3</li>
							</ol>
							<ol class="e-list">
								<li>List element 1</li>
								<li>List element 2</li>
								<li>List element 3</li>
							</ol>
							<a class="e-link" href="mailto:info@unibit.lv">info@unibit.lv</a><br />
							<a class="e-link" href="tel:+37128351324">+37128351324</a><br />
							<a class="e-link" href="skype:username?call">skype:eif_artur</a><br />
							<a class="e-link" href="#">Google map</a><br />
							<a class="e-link" href="#">Attachment</a><br /><br/>
							<button class="e-btn e-btn-primary">normal</button>
							<table class="table table-striped e-table e-table-bordered">
								<caption>таблица роста стоимости</caption>
								<thead>
									<tr>
										<th>Заголовок таблицы</th>
										<th>Заголовок таблицы</th>
										<th>Заголовок таблицы</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1-25</td>
										<td>Текст в таблице Текст в таблице  Текст в таблице</td>
										<td>1-25</td>
									</tr>
									<tr>
										<td>1-25</td>
										<td></td>
										<td>1-25</td>
									</tr>
									<tr>
										<td>1-25</td>
										<td></td>
										<td>1-25</td>
									</tr>
								</tbody>
							</table>
							<hr />
							<h1>Heading 1</h1>
							<h2>Heading 2</h2>
							<h3>Heading 3</h3>
							<h4>Heading 4</h4>
							<h5>Heading 5</h5>
							<h6>Heading 6</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mt-5 e-mb-180">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<h2 class="mb-5">Palet</h2>
						</div>
						<div class="col-lg-8">
							<div class="panel-group e-panel e-panel-black" id="Panel">
								<div class="panel e-active">
									<div class="panel-heading" data-toggle="collapse" data-parent="#Panel" href="#panel1">
										<span class="panel-date">2019 May</span>
										<h4 class="panel-title">
											Panel Title
										</h4>
									</div>
									<div id="panel1" class="panel-collapse collapse in show">
										<div class="panel-body">
											Panel Text........										
										</div>
									</div>
								</div>
								<div class="panel">
									<div class="panel-heading" data-toggle="collapse" data-parent="#Panel" href="#panel2">
										<span class="panel-date">2019 May</span>
										<h4 class="panel-title">Panel Title</h4>
									</div>
									<div id="panel2" class="panel-collapse collapse in">
										<div class="panel-body">
											Panel Text........
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#Panel" href="#panel3">
										<span class="panel-date">2019 May</span>
										<h4 class="panel-title">Panel Title</h4>
									</div>
									<div id="panel3" class="panel-collapse collapse in">
										<div class="panel-body">
											Panel Text........
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include 'shared/_footer.php';?>
		<script src="/js/global.js"></script>
  	</body>
</html>