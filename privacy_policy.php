<?php require '_global.php';?>
<!doctype html>
<html lang="en">
	<head>
		<title>Privacy Policy</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Bootstrap CSS -->
        <!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
        <link rel="stylesheet" href="/css/house.css">
        <link rel="stylesheet" href="/lightBox/css/lightbox.css">
		<!--END CSS -->
		<!--START JS -->
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->      
	</head>
	<body>
        <?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
        <?php include 'shared/_termometerWindowForm.php';?>
        <div class="container mt-5">
            <div class="row e-mb-100">
                <div class="col-md-4">
                    <h2 class="e-text-anime mb-5">Privacy Policy</h2>
                </div>
                <div class="col-md-8 e-text-anime">
                    Information comming soon.
                </div>
            </div>
        </div>
        <?php include 'shared/_footer.php';?>
        <!-- START JS -->
        <script src="/js/global.js"></script>
        <script src="/js/termometer.js"></script>
        <script src="/lightBox/js/lightbox.js"></script>
        <!-- END JS -->
  </body>
</html>