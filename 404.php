<?php require '_global.php';?>
<!doctype html>
<html lang="en">
  	<head>
		<title>Page not found</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/global.css">
        <style>
            .error-container{
                position: fixed;
                height: 100%;
                display: flex;
                flex-direction: column;
                justify-content: center;
                color: white;
            }
            h2{
                color: white;
                font-weight: normal;
            }
            h1{
                font-size: 72px;
                color: #FFF;
                font-weight: 600;
                letter-spacing: 10px;
            }
        </style>
	</head>
	<body>
        <div class="error-container container-fluid e-bg-dark pl-sm-5">
            <div class="container">
                <h1>404</h1>
                <h2 class="pb-3">Страница не найдена</h2>
                <a href="/" class="e-btn e-btn-primary" style="width: 220px !important; color:black;">вернуться k главный</a>
            </div>
        </div>
    </body>

</html>