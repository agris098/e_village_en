<?php require '_global.php';?>
<!doctype html>
<html lang="en">
  	<head>
		<title>Contacts</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
		<!--END CSS -->
		<!--START JS -->
		<script src="/js/jquery-3.4.1.min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->
	</head>
	<body>
		<?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
		<?php include 'shared/_termometerWindowForm.php';?>
		<div class="container-fluid mt-5 e-mb-180">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 e-text-anime">
                            <h2 class="mb-5">Contact information</h2>
                            <p>
                                SIA E-Village<br />
                                Street Brivibas 1, Riga, LV-1000,<br />
                                LATVIA
                            </p>
						</div>
						<div class="col-lg-8 e-text-anime">
							<form action="#" method="POST" class="contact-form contact-form-white validate">
								<div class="form-group">
									<input type="text" name="name" class="form-control form-control-lg" placeholder="Name" required>
								</div>
								<div class="form-group">
									<input type="email" name="email" class="form-control form-control-lg" placeholder="E-mail" required>
								</div>
								<div class="form-group">
									<textarea type="" name="message" class="form-control form-control-lg" placeholder="Message" required></textarea>
								</div>
								<div class="custom-control custom-checkbox">
									<input type="checkbox" name="terms" class="custom-control-input" checked="checked" id="customCheck">
									<label class="custom-control-label" for="customCheck">I give my consent to the processing of personal data</label>
								</div>
								<button class="mt-4 e-btn e-btn-dark-long">SEND REQUEST</button>
								<a class="mt-4 mb-3" href="/privacy_policy">Privacy policy</a>
								<a href="/terms_of_use">Terms of Use</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include 'shared/_modalThanks.php';?>
		<?php include 'shared/_footer.php';?>
		<script src="/js/global.js"></script>
		<script src="/js/termometer.js"></script>
  	</body>
</html>