<?php require '_global.php';?>
<!doctype html>
<html lang="en">
  	<head>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-5HVW43D');</script>
		<!-- End Google Tag Manager -->
		<title>E-Village</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">

		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
		<link rel="stylesheet" href="/css/home.css">
		<!--END CSS -->
		<!--START JS -->
		<script src="/js/jquery-3.4.1.min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.waypoints.min.js"></script>
		<script src="/js/objectFitPolyfill.js"></script>
		<!--END JS -->
	</head>
	<body class="preloader-site">
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HVW43D" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<!-- Start  ---------------- Preloader -------------- -->
		<div class="preloader-wrapper">
			<div class="preloader">
				<div class="container"><text></text></div>
			</div>
		</div>
		<!-- End  ---------------- Preloader -------------- -->
		<?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_contactWindowForm.php';?>
		<?php include 'shared/_termometerWindowForm.php';?>
		<div class="e-home-header-wrapper">
			<div class="e-termometer">
				<div class="e-termometer-container">
					<div style="display: flex; align-items: center;height: 100%;">
						<div class="container">
							<div class="row">
								<div class="col-md-5 left-c">
									<p class="house-c">28</p>
									<p class="house-d">number of reserved land plots</p>
								</div>
								<div class="col-md-7">
									<p class="e-termometer-table-header">price termometer</p>
									<div class="e-termometer-table-container">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="e-termometer-btn">
					<span class="text text-uppercase font-white">Price termometer</span>
				</div>
			</div>
			<div class="e-home-header-content">
				<header class="e-header position-absolute w-100">
					<nav class="container navbar navbar-expand-xl">
						<div class="row h-100">
							<div class="col-3">
								<a class="navbar-brand" href="/"><img src="/img/svg/menu_logo.svg"></a>
							</div>
							<div class="col-9">
								<div class="collapse navbar-collapse" id="collapsibleNavId">
									<ul class="navbar-nav mt-2 mt-lg-0 w-100">
										<li class="nav-item">
											<ul class="e-nav-list nav-link">
												<li>
													<a href="/village_map">Village Map</a>
												</li>
												<li><a href="/houses">House projects</a></li>
												<li><a href="/price">Price and procedure</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/road_map">Road map</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/news">News</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/faq">FAQ</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="/contact">Contacts</a>
										</li>
										<div style="display: flex;flex: auto;"></div>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#" id="dropdownId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EN</a>
											<div class="dropdown-menu lang" aria-labelledby="dropdownId2">
												<a class="dropdown-item" href="<?php echo $ruRequestUri;?>">RU</a>
												<!-- <a class="dropdown-item" href="#">LV</a> -->
											</div>
										</li>
									</ul>
								</div>
								<button class="e-mobile-menu-open navbar-toggler hidden-lg-up pull-right border-0 p-0" type="button">
									<img class="hidden-lg-up pull-right" src="/img/svg/ic_burger_white.svg">
								</button>
							</div>
						</div>
					</nav>
				</header>
				<div id="main-slider" class="carousel slide e-slider e-slider-home carousel-fade" data-interval="false">
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="e-slider-content m-auto text-center" style="z-index: 60;">
								<img style="margin: -7px;" src="/img/svg/logo_slider.svg">
								<h2 style="color: #FFF; font-weight: normal;">The first crypto village in Europe</h2>
								<button class="e-btn e-btn-primary mt-4 e-contact-window-open">Leave request</button>
							</div>
							<div class="e-slider-overlay"></div>
							<video data-object-fit="cover" style="position: absolute;right: 0;bottom: 0;object-fit: cover;width: 100%;height: 100%;" data-object-position="50% 50%" autoplay muted loop id="myVideo">
								<source src="/videos/e_village_bg.webm" type="video/webm">
								<source src="/videos/e_village_bg.ogv" type="video/ogv">
								<source src="/videos/e_village_bg.mp4" type="video/mp4">
							</video>
						</div>
						<div class="carousel-item" style="background-image: url(/img/jpg/Slider.jpg);">
							<div class="e-slider-content m-auto text-center">
								<h1 class="e-slider-h1">Fachwerk House</h1>
								<p class="e-slider-p">
									Two-story houses using fachwerk technology
								</p>
								<a href="/houses" class="e-btn e-btn-primary">View projects</a>
							</div>
						</div>
					</div>
					<!--<div class="slider-unique-visits e-text-small text-white">
						Unique visits:<br />
						<span>0003100</span>
					</div>   -->
					<!-- Left and right controls -->
					<div class="slider-controls d-flex align-items-center justify-content-center">
						<div class="slider-bar">
							<div class="slider-bar-fill"></div>
						</div>
						<a class="h-100 align-items-center justify-content-center d-flex" href="#main-slider" data-slide="prev">
							<img src="/img/svg/arrow_slider_left.svg">
						</a>
						<span class="slide-counter">1 / 6</span>
						<a class="h-100 align-items-center justify-content-center d-flex" href="#main-slider" data-slide="next">
							<img src="/img/svg/arrow_slider_right.svg">
						</a>
					</div>
					<div class="social-media-container">
						<ul class="list-unstyled m-0">
							<li><img src="/img/svg/ic_instagram.svg"></li>
							<li><img src="/img/svg/ic_facebook.svg"></li>
							<li><img onclick="window.open('https://www.youtube.com/channel/UCEnctkA1cRcqaXwGdHW7Few/featured');" src="/img/svg/ic_youtube_white.svg"></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container e-mtb-180 e-scroll-anime">
			<div class="row">
				<div class="col-lg-4">
					<h2 class="e-text-anime mb-5">About Village</h2>
				</div>
				<div class="col-lg-8">
					<p class="e-text-anime">
					<b>E-Village</b> is modern hi-tech village that is created specifically for crypto society and people who would like increase level of life standards. Village located in Northern part of The Latvian capital – Riga near satellite town Garciems of Carnikava region. Only 800 meters from the Baltic Sea. 

Village will contain between 120 – 140 houses of high quality surrounded by nature. Development team planning well prepared inside infrastructure like kindergarten, tennis court, basketball and football pitch, crypto grocery store, hotel for seminars and facility for IT school. Also, office sharing and car sharing for residents Hi-tech electricity and security supply in the Village.
					</p>
					<div class="row">
						<div class="col-6 col-sm-3 e-text-anime">
							<div><span class="e-large">125</span></div>
							<p class="e-semi-bold">houses of high quality</p>
						</div>
						<div class="col-6 col-sm-3 e-text-anime">
							<div class="m-0 d-block"><span class="e-large">15</span>ha</div>
							<p class="e-semi-bold">land area</p>
						</div>
						<div class="col-6 col-sm-3 e-text-anime">
							<div><span class="e-large">10</span>min</div>
							<p class="e-semi-bold">drive to OZO GOLF CLUB</p>
						</div>
						<div class="col-6 col-sm-3 e-text-anime">
							<div><span class="e-large">25</span>min</div>
							<p class="e-semi-bold">from Riga International Airport</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="e-googleMap-wrapper e-bg-dark">
			<div class="e-googleMap-content">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="e-googleMap-image"></div>
							<div class="e-image-anime right e-bg-dark"></div>
						</div>
						<div class="col-lg-6  e-mtb-180">
							<h2 class="pb-5 text-white e-text-anime">
								Location
							</h2>
							<p class="text-white e-text-anime"> 
								Target of project is to combine community, nature, digital technologies with view to increase life standards. 
								Future residents can purchase their future houses with own cryptocurrency.
							</p>
							<a class="e-btn mt-5 e-btn-white e-btn-mob e-text-anime e-googleMap-toggle" >Google maps</a>
						</div>
					</div>
				</div>
			</div>
			<div class="e-googleMap-map">
				<div class="mapouter">
					<!-- Start ---  Google Maps Api -->
					<!-- <div id="e-googleMap-api" class="gmap_canvas"></div> -->
					<!-- END ---  Google Maps Api -->
					<!-- Start --- Google Map Embed version  -->
					<div class="gmap_canvas">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d69370.58316514708!2d24.150851126536004!3d57.088653578682084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNTfCsDA1JzM3LjAiTiAyNMKwMTEnMzcuOCJF!5e0!3m2!1slv!2slv!4v1566039526871!5m2!1slv!2slv" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<!-- END --- Google Map Embed version  -->
				</div>
				<a class="btn-close e-googleMap-toggle"></a>
			</div>
		</div>  
		<!-- <div class="container-fluid bg-primary p-0 d-none d-md-block">
			<div class="videoWrapper">
				<iframe width="560" height="349" src="https://www.youtube.com/embed/wbkQsJdBQNc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div> -->
		<div class="container e-mtb-180">
			<div class="row align-items-center">
				<div class="col-lg-4 mt-lg-0 mt-5 order-2 order-lg-1">
					<h2 class="pb-5 e-text-anime">Houses</h2>
					<p class="e-text-anime">
						Village will have same style and standards for buildings design.
						All houses will build with fachwerk traditions. Top technologies inside will help residents control its houses through telephone application. 

						Size of houses between 160 to 200 squire meters. Only ecological materials will be used with way 
						of fashion.
					</p>
					<a class="e-btn e-btn-dark e-btn-mob mt-5 e-text-anime" href="/houses" role="button">View projects</a>
				</div>
				<div class="col-lg-8 order-1 order-lg-2">
					<div class="e-slider-mini-wrapper">
						<div id="mini-slider" class="carousel slide e-slider e-slider-mini carousel-fade" data-ride="carousel">
							<!-- The slideshow -->
							<div class="carousel-inner">
								<div class="carousel-item active" style="background-image: url(/img/jpg/home/hemnic/hemnic.jpg);">
								</div>
								<div class="carousel-item" style="background-image: url(/img/jpg/home/hemnic/hemnic-1.jpg);">
								</div>
								<div class="carousel-item" style="background-image: url(/img/jpg/home/hemnic/hemnic-2.jpg);">
								</div>
							</div>
							<!-- Left and right controls -->
							<div class="slider-controls d-flex align-items-center justify-content-center">
								<a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider" data-slide="prev">
									<img src="/img/svg/arrow_slider_left.svg">
								</a>
								<span class="slide-counter">1 / 6</span>
								<a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider" data-slide="next">
									<img src="/img/svg/arrow_slider_right.svg">
								</a>
							</div>
						</div>
					</div>
					<div class="e-image-anime e-bg-white"></div>
				</div>
			</div>
		</div>
		<div class="e-bg-light-grey e-ptb-180">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-8 order-lg-1">
						<div class="videoWrapper">
							<iframe width="560" height="349" src="https://www.youtube.com/embed/wbkQsJdBQNc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<div class="e-image-anime right e-bg-light-grey"></div>
						</div>
					</div>
					<div class="col-lg-4 mt-5 order-lg-2">
						<h2 class="e-text-anime">Nature</h2>
						<p class="e-text-anime mt-5">The sheer volume of wild nature makes Latvia one of the greenest countries in the world. Ironically one may say that half of Latvia is not covered by lush forests. The country holds one of the rarest ecosystems of the world largely untouched by civilization. It is a haven for the visitor who seeks to experience a land where nature and tradition have coexisted in harmony from time immemorial.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container e-mtb-180">
			<div class="row align-items-center">
				<div class="col-lg-8 order-lg-2 mb-lg-0 mb-5">
					<img src="/img/jpg/security.jpg" class="w-100">
					<div class="e-image-anime e-bg-white"></div>
				</div>
				<div class="col-lg-4 order-lg-1">
					<h2 class="mb-5 e-text-anime">Security</h2>
					<p class="e-text-anime">
					E-Village’s own security service company will  provide round-the-clock monitoring of the village. Comprehensive control of all security systems.					</p>
				</div>
			</div>
		</div>
		<div class="container-fluid e-bg-dark">
			<div class="row e-bg-dark">
				<div class="container e-mtb-180">
					<div class="row">
						<div class="col-lg-4">
							<h2 class="mb-5 text-white e-text-anime">Infrastructure</h2>
						</div>
						<div class="col-lg-8">
							<p class="text-white e-text-anime">
								Stroll back to when less time was spent commuting  from work, and more quality moments were spent close of nature and sea  close to convinient comfort of home.  The Sea Village welcomes this thought back to North Riga unleashing a modern take on luxury house living. 

								Treating residents to variety of different type of houses.

								Village will contain in its premisses grocery store, restaurant, hotel, SPA and fitness, swimming pool, tennis court, football and basketball facilities.  Kinder garten, office space, car sharing and more.  Security will apply video, face recognition, biometrics and guards.							
							</p>
							<div class="panel-group e-panel mt-5" id="accordion">
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
									<h4 class="panel-title">
										Improvement
									</h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse in">
										<div class="panel-body">
											Landscape design, lights, walking space and other internal comforts.
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
									<h4 class="panel-title">
										Electricity
									</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse">
										<div class="panel-body">
											Стабильные поставки от надёжного поставщика будут субсидироваться собственными  источниками и мощностями экологически дружественной и эффективной электроэнергии.
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
									<h4 class="panel-title">
										Heat supply
									</h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse">
									<div class="panel-body">
											Индивидуальные тепловые  насосы для каждого дома, и тёплые полы являются приоритетными подходами в решении вопроса теплоснабжения в посёлке.
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
									<h4 class="panel-title">
										Water supply
									</h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse">
										<div class="panel-body">
											Муниципальное-региональное предприятие будет централизованным поставщиком по водоснабжению посёлка.
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
									<h4 class="panel-title">
										Освещение улиц
									</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse">
										<div class="panel-body">
											Высокое качество.
										</div>
									</div>
								</div>
								<div class="panel e-panel-anime">
									<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
									<h4 class="panel-title">
										Дороги
									</h4>
									</div>
									<div id="collapse6" class="panel-collapse collapse">
										<div class="panel-body">
											Асфальт, брусчатка или пластиковые.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="e-bg-dark d-none d-sm-block">
		<!-- 	<div class="e-slider-mini-wrapper"> -->
				<div id="mini-slider2" class="carousel slide e-slider e-slider-mini e-bg-fixed carousel-fade e-text-anime" data-ride="carousel">
					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active" style="background-image: url(/img/jpg/home/hemnic/hemnic.jpg);">
						</div>
						<div class="carousel-item" style="background-image: url(/img/jpg/home/hemnic/hemnic-1.jpg);">
						</div>
						<div class="carousel-item" style="background-image: url(/img/jpg/home/hemnic/hemnic-2.jpg);">
						</div>
					</div>
					<!-- Left and right controls -->
					<div class="slider-controls d-flex align-items-center justify-content-center">
						<a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider2" data-slide="prev">
							<img src="/img/svg/arrow_slider_left.svg">
						</a>
						<span class="slide-counter">1 / 6</span>
						<a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider2" data-slide="next">
							<img src="/img/svg/arrow_slider_right.svg">
						</a>
					</div>
				</div>
<!-- 			</div> -->
		</div>
		<div class="e-ptb-180 e-bg-light-grey">
			<div class="container e-price-container">
				<div class="row mb-5">
					<div class="col-12">
						<h2 class="e-text-anime">Price</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 mb-lg-0 mb-5 px-xl-4 e-text-anime">
						<div class="card e-border mb-3 e-shadow e-card h-100 ">
							<img class="e-card-icon e-modal-price-open-1" src="/img/svg/ic_info.svg">
							<div class="card-header e-card-header border-0 e-card-title">
								Without EU resident visa
							</div>
							<div class="card-body">
								<div class="e-card-border pt-3">
									<h2>15,000 €</h2>
									<p>Reservation fee</p>
								</div>
								<p class="pt-3">up to</p>
								<span class="e-card-title">2,000,000 Dagcoins</span>
							</div>
							<div class="e-card-footer p-0 border-0">
								<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-1">More</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 mb-lg-0 mb-5 px-xl-4 e-text-anime">
						<div class="card e-border mb-3 e-card h-100 e-shadow">
							<img class="e-card-icon e-modal-price-open-2" src="/img/svg/ic_info.svg">
							<div class="card-header e-card-header border-0 e-card-title">
								With EU residents visa
							</div>
							<div class="card-body">
								<div class="e-card-border pt-3">
									<h2>75,000 €</h2>
									<p>Reservation fee</p>
								</div>
								<p class="pt-3">up to</p>
								<span class="e-card-title">1,750,000 Dagcoins</span>
							</div>
							<div class="e-card-footer p-0 border-0">
								<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-2">More</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 mb-lg-0 px-xl-4 e-text-anime">
						<div class="card mb-3 e-shadow e-border e-card h-100">
							<img class="e-card-icon e-modal-price-open-3" src="/img/svg/ic_info.svg">
							<div class="card-header e-card-header border-0 e-card-title">
								Program for EU Residents Permit
							</div>
							<div class="card-body">
								<div class="e-card-border pt-3">
									<h2>75,000 €</h2>
									<p>Investment</p>
								</div>
								<p class="pt-3">
									For those who not involved with crypto business, but has interest receive 5 years Residents Visa in EU for itself and members of family our project can create proper  solution.
								</p>
							</div>
							<div class="e-card-footer p-0 border-0">
								<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-3">More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- 		<div class="container e-ptb-180">
			<div class="row align-items-center">
				<div class="col-12 order-2 order-md-1">
					<h2 class="e-text-anime mb-5">Project partners</h2>
				</div>
			</div>
			<div class="row e-partners">
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_mi.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_fire.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_apple.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_nike.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_mi.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_fire.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_apple.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_nike.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_mi.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_fire.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_apple.svg"></div>
				<div class="col-6 col-md-3 pt-5 e-text-anime"><img src="/img/svg/ic_partner_nike.svg"></div>
			</div>
		</div>
		<div class="container-fluid e-bg-dark">
			<div class="row e-bg-dark">
				<div class="container e-mtb-180">
					<div class="row">
						<div class="col-lg-4">
							<h2 class="mb-5 text-white e-text-anime">Fill out an request</h2>
						</div>
						<div class="col-lg-8">
							<form action="#" method="POST" class="contact-form e-text-anime validate">
								<div class="form-group">
									<input type="text" name="name" class="form-control form-control-lg" placeholder="Name" required>
								</div>
								<div class="form-group">
									<input type="email" name="email" class="form-control form-control-lg" placeholder="E-mail" required>
								</div>
								<div class="form-group">
									<input type="text" name="country" class="form-control form-control-lg" placeholder="Country" required>
								</div>
								<div class="form-group">
									<textarea type="" name="message" class="form-control form-control-lg" placeholder="Message" required></textarea>
								</div>
								<div class="custom-control custom-checkbox">
									<input type="checkbox" name="terms" class="custom-control-input" checked="checked" id="customCheck2">
									<label class="custom-control-label" for="customCheck2">I give my consent to the processing of personal data</label>
								</div>
								<button class="mt-4 e-btn e-btn-light-long">SEND REQUEST</button>
								<a class="mt-4 mb-3" href="/privacy_policy">Privacy policy</a>
								<a href="/terms_of_use">Terms of Use</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<?php include 'shared/_footer.php';?>
		<?php include 'shared/_modalPrice1.php';?>
		<?php include 'shared/_modalPrice2.php';?>
		<?php include 'shared/_modalPrice3.php';?>
		<?php include 'shared/_modalThanks.php';?>
		
		<!-- START JS -->
		<script src="/js/global.js"></script>
		<script src="/js/termometer.js"></script>
		<script src="/js/home.js"></script>
		<script src="/js/google_map.js"></script>
		<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLMaz-2mwgd7eA1O3IHQZx6EdEDtf6118&callback=initMap"></script> -->
		<!-- END JS -->
  	</body>
</html>