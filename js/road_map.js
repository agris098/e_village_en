$(function(){
    function RoadMap(){
        var _$container;
        this.init = function($container){
            getHeadTemplate(function(template){
                getHeadData(function(headData){
                    _$container = $container;
                    console.log(headData);
                    var data = headData;
                    // START - filter Date values
                    var values = _.map(_.sortBy(data, function(i){ return new Date(i.Year, i.Month-1, 1,0,0,0,0);}) , function(group) {
                        return { Year: group.Year, Month: group.Month, Title: getDateString(group.Year, group.Month)} // convert Date to String
                    });
                    var keys = _.unique(values, function(v){return v.Title});
                    // END - filter Date values
        
                    // init, add template
                    var list = keys;//.concat(keys, keys);
                    var utmp = _.template(template);
                    var $template = $(utmp({list: list}));
                    $container.append($template);

                    registerEvents();
                    setTitleActive($template, true);
                });
            });
        }
        function getHeadData(clb){
            $.ajax({
                url: '/temp_data/road_map_header.php',
                cache: false,
                success: clb,
                error: function(xhr){
                    console.error(xhr);
                }
            });
        }
        function getBodyData(date, clb){
            $.ajax({
                url: '/temp_data/road_map_body.php',
                cache: false,
                success: clb,
                data: date,
                error: function(xhr){
                    console.error(xhr);
                }
            });
        }
        function getHeadTemplate(cbl){
            $.ajax({
                url: '/templates/roadmap_header.html',
                cache: true,
                success: cbl
            });
        }
        function getBodyTemplate(cbl){
            $.ajax({
                url: '/templates/roadmap_body.html',
                cache: true,
                success: cbl
            });
        }
        function returnClosestDate($titleList){
            var obj,
                nowDate = new Date();    
            $.each($titleList, function(i, v){
                var data = $(v).data('date');
                 if(getYMTime(data.Year, data.Month - 1) <= getYMTime(nowDate.getFullYear(), nowDate.getMonth())){
                    obj = v;
                }else{
                    if(typeof(obj) != 'undefined')
                        return false;
                    obj = v;
                }
            });
            return obj;
        }
        function setTitleActive($template, noAnimation){
            var $titleList = $template.find('ul li');
            var $el = $(returnClosestDate($titleList));
            $el.addClass('active');
            setOffsetForActive($el, noAnimation);
            loadRoadMapBody($el.data('date'), noAnimation);
        }
        function getYMTime(year, month){
            // Month should be from 0
            return new Date(year, month,1,0,0,0,0).getTime();
        }
        function registerEvents(){
            var curDown = false,
                curYPos = 0,
                curScrollLeft = 0,
                noClick = false;
            _$container.on('click', 'ul li', function(e){
                if(noClick){
                   noClick = false;
                } else {
                    var $this = $(this);
                    $this.parent().find('li.active').removeClass('active');
                    $this.addClass('active');
                    setOffsetForActive($this);
                    loadRoadMapBody($this.data('date'));
                }
            });
            _$container.on('click', '[data-rmap=next]', function(e){
                nextTitle();
            });
            _$container.on('click', '[data-rmap=prev]', function(e){
                previousTitle();
            });
            $(window).resize(function(){
                var $active = _$container.find('ul li.active');
                setOffsetForActive($active);
            });
            // Start - Header Scroll events
            _$container.on('mouseup mouseleave touchend', '.e-rmap-head-content', function(e){
                curDown = false;
            });
            _$container.on('mousemove', '.e-rmap-head-content', function(e){
                if(curDown === true){
                    var offset = curXPos - e.pageX;
                    if(Math.abs(offset) > 5){
                        noClick = true;
                    }
                    var $con = $(this).find('ul');
                    $con.scrollLeft(curScrollLeft + offset);
                }
            });
            _$container.on('touchmove', '.e-rmap-head-content', function(e){
                var touch = e.touches[0];
                if(curDown === true){
                    var offset = curXPos - touch.pageX;
                    if(Math.abs(offset) > 5){
                        noClick = true;
                    }
                    var $con = $(this).find('ul');
                    $con.scrollLeft(curScrollLeft + offset);
                }
            });
            _$container.on('mousedown', '.e-rmap-head-content', function(e){
                curDown = true;
                curXPos = e.pageX;
                noClick = false;
                curScrollLeft = $(this).find('ul').scrollLeft();
            });
            _$container.on('touchstart', '.e-rmap-head-content', function(e){
                var touch = e.touches[0];
                curDown = true;
                curXPos = touch.pageX;
                noClick = false;
                curScrollLeft = $(this).find('ul').scrollLeft();
            });
            // END - Header Scroll events
        }
        function nextTitle(){
            scroll(-1.5);
        }
        function previousTitle(){

            scroll(1.5);
        }
        function setOffsetForActive($el, noAnimation){
            var offset = $el[0].offsetLeft;
            // Animate 
            if(noAnimation){
                $el.parent().scrollLeft(offset - 30);
            } else {
                $el.parent().stop().animate({scrollLeft: offset - 30}, 500);
            }
        }
        function scroll(directionVector){

            var $con = _$container.find('ul'),
                offset = $con.children().first()[0].scrollWidth;
            // Animate
            $con.stop().animate({scrollLeft: $con[0].scrollLeft - (offset*directionVector)}, 500);
            // No animation $el.parent().scrollLeft(offset - 30);
        }
        function loadRoadMapBody(date, noAnimation){
            getBodyTemplate(function(template){
                getBodyData(date, function(data){
                    var utmp = _.template(template),
                        $bodyCon = $('.e-rmap-body-container'),
                        $body = $(utmp({data: data}));
                    $bodyCon.empty().append($body);
                    // show first element
/*                     var $firstPanel = $body.find('.panel').first();
                    $firstPanel.addClass('e-active');
                    $firstPanel.find('.panel-collapse').addClass('show'); */
                    reloadAnimation();
                    // run animation on elements if any
/*                     if(noAnimation){                      
                        reloadAnimation();
                    } else {
                        $body.find('.e-panel-anime').removeClass('e-panel-anime');
                    } */
                });
            });
        }
        function getDateString(Year, Month){
            var m = moment(new Date(Year, Month - 1));
            return m.format("YYYY MMM");
        }
    }
    var roadMap = new RoadMap();
    roadMap.init($('.e-rmap-head-container'));
});