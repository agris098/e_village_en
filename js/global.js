$(function(){
    $('.e-slider').carousel({
        interval: 5000
    });
    $('.e-slider').bind('slid.bs.carousel', function() {
        var $slider = $(this),
            $contentC = $(this).find('.carousel-inner');
        currentIndex = $contentC.find('.carousel-item.active').index() + 1;
        totalCount = $contentC.find('.carousel-item').length;
        $slider.find('.slide-counter').html(currentIndex + ' / ' + totalCount);
        var fillPct = (currentIndex / totalCount) * 100;
        var $progress = $slider.find('.slider-bar-fill');
        $progress.stop();
        $progress.css('width', '0px');
        $progress.animate({width: "100%"}, 5 * 1000, function(){
            $progress.css('width', '0px');
        });
    });
    $('.e-slider').trigger('slid.bs.carousel');
    $(document).on('click', '.e-panel [data-toggle = collapse]', function(){
        var $el = $(this);
        if($el.attr('aria-expanded') !== "false" ){
            $el.closest('.panel').addClass('e-active');
        } else {
            $el.closest('.panel').removeClass('e-active');
        }
    });
    // ================= Start --- Mobile Menu ===============
    var $mobileMenu = $('#e-mobile-menu');
    var $mobileMenuC = $mobileMenu.find('.content');
    $(document).on('click', '.e-mobile-menu-open, #e-mobile-menu-close', function(){
        $mobileMenu.toggleClass('show');
    });
    // adjust size for mobile
    $mobileMenuC.css({ height: window.innerHeight - 100 });
    $(window).on('resize', function () {
        $mobileMenuC.css({ height: window.innerHeight - 100 });
    });
    // ============== END --- Mobile Menu ==================
    var $contactWindow = $('#e-contact-window');
    $(document).on('click', '.e-contact-window-open, .e-contact-window-close', function(){
        $('.modal').modal('hide');
        $contactWindow.toggleClass('show');
    });
    var $termometerWindow = $('#e-termometer-window');
    $(document).on('click', '.e-termometer-window-open, .e-termometer-window-close', function(){
        $termometerWindow.toggleClass('show');
    });
    $('.e-termometer .e-termometer-btn').on('click', function(e){
		var $con = $(this).parent()
        $con.toggleClass('show');
		
		if($con.hasClass('show'))
			$.homeTermometer.ReFillBar();
    });
    $('.e-price-container').on('click', '[class*="e-modal-price-open"]', function(e){
        var modalTag;
        console.log($(this).attr('class'));
        if($(this).hasClass('e-modal-price-open-1'))
            modalTag = '-1'
        else if($(this).hasClass('e-modal-price-open-2'))
            modalTag = '-2'
        else
            modalTag = '-3'

        var $modal = $('#e-modal-price'+ modalTag);
        $modal.modal('show');
    });
    $(document).ready(function(){
        reloadAnimation();
    });
})
function reloadAnimation(){
    function triggerAnime($el){
        $el.addClass('load');
    }
    $('.e-text-anime').not('.load').waypoint(function(direction) {
        var $el = $(this.element),
            delay = parseInt($el.data('delay'));
        this.destroy();
        if(delay > 0){
            setTimeout(function(){ triggerAnime($el) }, delay);
        } else {
            triggerAnime($el);
        }
      }, {
          offset: '95%'
    });
    $('.e-image-anime').not('.load').waypoint(function(direction) {
        var $el = $(this.element),
            delay = parseInt($el.data('delay'));
        this.destroy();
        if(delay > 0){
            setTimeout(function(){ triggerAnime($el) }, delay);
        } else {
            triggerAnime($el);
        }
      }, {
          offset: '80%'
    });
    var panelDelay = 0;
    $('.e-panel-anime').not('.load').waypoint(function(direction) {
        var $el = $(this.element),
            delay = parseInt($el.data('delay'));
        panelDelay+=150;
        this.destroy();
        setTimeout(function(){ triggerAnime($el), panelDelay -= 150; }, panelDelay);
      }, {
          offset: '90%'
    });
}
function showThanksModal(){
    $('#e-modal-thanks').modal('show');
}
// ----------------------- Preloader -----------------------
$(window).on('load',function() {
    setTimeout(function(){
        $('.preloader-wrapper').animate({top: '-100%' },500, function(){
            $('body').removeClass('preloader-site');
        });
    }, 1000);
});
// ----------------------- Preloader -----------------------
// ----------------------- Contact Form --------------------
$(function(){
    // Start - Override default Email validation 
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    $.validator.methods.email = function( value, element ) {
        return this.optional( element ) || isValidEmailAddress(value);
    }
    // END - Override default Email validation 
    $.each($('.validate'), function(i,v) {
        $(v).validate({
            rules: {
                terms: "required"
            }
        });
    });
    const lang = document.documentElement.lang || 'en';
    if(lang !== 'en'){
        const langM = {
            ru: {
                required: "Это поле обязательно к заполнению.",
                remote: "Пожалуйста, исправьте это поле.",
                email: "Пожалуйста, введите действительный адрес электронной почты.",
                url: "Пожалуйста, введите корректный адрес.",
                date: "Пожалуйста, введите правильную дату.",
                dateISO: "Пожалуйста, введите действительную дату (ISO).",
                number: "Пожалуйста, введите корректное число.",
                digits: "Пожалуйста, вводите только цифры.",
                creditcard: "Пожалуйста введите действующий номер кредитной карты.",
                equalTo: "Пожалуйста, введите то же значение снова.",
                accept: "Пожалуйста, введите значение с допустимым расширением.",
                maxlength: jQuery.validator.format("Пожалуйста, введите не более {0} символов."),
                minlength: jQuery.validator.format("Пожалуйста, введите не менее {0} символов."),
                rangelength: jQuery.validator.format("Пожалуйста, введите значение от {0} до {1} символов."),
                range: jQuery.validator.format("Пожалуйста, введите значение между {0} и {1}."),
                max: jQuery.validator.format("Пожалуйста, введите значение, меньшее или равное {0}."),
                min: jQuery.validator.format("Пожалуйста, введите значение, большее или равное {0}.")
            }
        };
        const customMessage = langM[lang] ? langM[lang] : undefined;
        if(customMessage){
            jQuery.extend(jQuery.validator.messages, {
                required: customMessage.required,
                remote: customMessage.remote,
                email: customMessage.email,
                url: customMessage.url,
                date: customMessage.date,
                dateISO: customMessage.dateISO,
                number: customMessage.number,
                digits: customMessage.digits,
                creditcard: customMessage.creditcard,
                equalTo: customMessage.equalTo,
                accept: customMessage.accept,
                maxlength: customMessage.maxlength,
                minlength: customMessage.minlength,
                rangelength: customMessage.rangelength,
                range: customMessage.range,
                max: customMessage.max,
                min: customMessage.min
            });
        }
    }
    $('.contact-form').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        if($(this).valid()){
            $.ajax({
                type: 'POST',
                url: '/send_email.php',
                data: $form.serialize(),
                dataType: "json",
                success: function(resp){
                    if(resp.status === 'OK'){
                        $form[0].reset();
                        $form.closest('.e-contact-window').removeClass('show');
                        showThanksModal();
                    } else {
                        alert('Failed to send request!');
                        console.error(resp);
                    }
                },
                error: function(xhr){
                    console.error(xhr);
                }
            });
        };
    });
    $(document).on('click', 
        '.e-admin-container .collapse-panel > h1 > a, ' + 
        '.e-admin-container .collapse-panel > h2 > a, ' +
        '.e-admin-container .collapse-panel > h3 > a, ' + 
        '.e-admin-container .collapse-panel > h4 > a, ' +
        '.e-admin-container .collapse-panel > h5 > a, ' +
        '.e-admin-container .collapse-panel > h6 > a', function(){
        var $parent = $(this).closest('.collapse-panel');
        //var $content = $parent.find('.panel-content');
        $parent.toggleClass('show');
    });
});
// ----------------------- Contact Form --------------------
// ----------------------- mobile Menu button ----------------
var prevScrollpos = window.pageYOffset;
var $mobileMenuButton = $('.e-mobile-menu-button');
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos && window.pageYOffset > 100) {
    $mobileMenuButton.show();
  } else {
    $mobileMenuButton.hide();
  }
  prevScrollpos = currentScrollPos;
}
// ----------------------- mobile Menu button ----------------