function Termometer(){
    var _self = this,
        _$container,
        _$table,
        _$activeRow,
        _color = '#ECAE83';
            
    this.init = function(params){
        _$container = params.container;
        initTable();
    }
    function initTable(){
        getData(function(data){
            _$table = $(getTemplate());
            $.each(data,function(i,v){
                _$table.append(renderRow(data,i,v));
            });
            function renderRow(data,i,v){
                var bg_color = shadeColor(_color, ((-i/data.length + 0.5)/2.5) * 100),
                    $row;
                if(i == 0){
                    $row = $(
                        "<tr>" +
                            "<td style='background-color:"+bg_color+";'>"+v.Count+ "<br/><b>"+v.Price+"</b></td>" +
                            "<td rowspan='"+data.length+"'><div class='fill-bar'><div class='inner-fill'></div></div></div></td>"+
                            "<td>"+v.Price+"</td>"+
                        "</tr>");
                } else {
                    $row = $(
                        "<tr>"+
                            "<td style='background-color:"+bg_color+";'>"+v.Count+ "<br/><b>"+v.Price+"</b></td>" +
                            "<td>"+v.Price+"</td>"+
                        "</tr>");
                };
                if(v.Active)
                    $row.addClass('active');
                return $row;
            };
            _$container.append(_$table);
            _$activeRow = _$table.find('tr.active');
            fill_bar(_$activeRow);
            registerEvents();
        });
    }
    function registerEvents(){
        _$table.on('mouseenter', 'tr td:first-child', function(){
            fill_bar($(this).parent());
        });
        _$table.on('mouseleave', 'tr td:first-child', function(){
            fill_bar(_$activeRow);
        });
        $(window).resize( function(){
            fill_bar(_$activeRow);
        });
    }
    function getData(clb){
        $.ajax({
            url: '/temp_data/termometer.php',
            cache: false,
            success: clb,
            error: function(xhr){
                console.error(xhr);
            }
        });
    }
    function getTemplate(){
        return'<table class="table table-bordered"></table>';
    }
    function fill_bar($row){
        var table_height = _$table.height(),
            activeRow = $row,
            rowOffest = activeRow.get(0).offsetTop,
            rowHeight = activeRow.height(),
            $inner_fill = _$table.find('.fill-bar .inner-fill');
        $inner_fill.css('height', (table_height - rowOffest) - (rowHeight/2) + 'px');
        activeRow.parent().find('tr td:last-child').css('opacity', '0');
        activeRow.children().last().css('opacity', '1');
    }
	this.ReFillBar = function(){
		setTimeout(function(){
			fill_bar(_$activeRow);
		}, 500);
	}
    function shadeColor(color, percent) {

        var R = parseInt(color.substring(1,3),16);
        var G = parseInt(color.substring(3,5),16);
        var B = parseInt(color.substring(5,7),16);
    
        R = parseInt(R * (100 + percent) / 100);
        G = parseInt(G * (100 + percent) / 100);
        B = parseInt(B * (100 + percent) / 100);
    
        R = (R<255)?R:255;  
        G = (G<255)?G:255;  
        B = (B<255)?B:255;  
    
        var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
        var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
        var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));
    
        return "#"+RR+GG+BB;
    }
}
$(function(){
    var windowTermometer = new Termometer();
    windowTermometer.init({container: $('.e-termometer-window .e-termometer-table-container')});
});