(function ($){
/*
*	Update view height for Mobile device regarding Header
*
*/
    var $e_map = $('.e-village-map');
    // adjust size for mobile
    var media = window.matchMedia("(max-width: 1200px)").matches
    $e_map.css({ height: window.innerHeight - (media ? 100 : 200) });
    $(window).on('resize', function () {
        $e_map.css({ height: window.innerHeight - (window.matchMedia("(max-width: 1200px)").matches ? 100 : 200) });
    });
/*
*	Map Tooltip Labels
*/
    $.mapTooltipLabels = {
        1: 'Free',
        2: 'Bought',
        3: 'Reserved'
    };
/*  
*	Functions Used for Map
*/
    function getPopupData(houseId, clb){
        getPopupTemplate(function(template){
             $.ajax({
                url: '/temp_data/village_map_house.php',
                cache: false,
                data: { HouseId: houseId },
                success: function(data){
                    var utmp = _.template(template);
                    clb(utmp(data));
                },
                error: function(xhr){
                    console.error(xhr);
                }
            });
        })
        function getPopupTemplate(tclb){
            $.ajax({
                url: '/templates/village_map_popup.html',
                cache: true,
                success: tclb
            });
        }
    }
    function getHouses(clb){
        $.ajax({
            url: '/temp_data/village_map_houses.php',
            cache: false,
            success: clb,
            error: function(xhr){
                console.error(xhr);
            }
        });
    }
    function getIndications(clb){
        $.ajax({
            url: '/temp_data/village_map_indications.php',
            cache: false,
            success: clb,
            error: function(xhr){
                console.error(xhr);
            }
        });
    }
    function getCircleClassByStatus(status){
        switch(status){
            case 1: return 'e-status free';
            case 2: return 'e-status bought';
            case 3: return 'e-status reserved';
        }
    }
    function getTooltipTextByStatus(status){
       return $.mapTooltipLabels[status];
    }
    
    // Map possition
    var mapPosEnabled = window.location.hash.includes('mapPos');
    if(mapPosEnabled)
    {
        var $input = $("<input type='text' id='mapPosInput'/>").css(
            {
                'position': 'fixed',
                'bottom': '0',
                'z-index': '9999'
            }
        );
        $('body').append($input);
    }
    function updateMapPosInput(text){
        var copyText = document.getElementById('mapPosInput');
        copyText.value = text;
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/
        /* Copy the text inside the text field */
        document.execCommand("copy");
    }
/*
*	Here we extend the imgViewer widget to display markers and notes
*
* This can be done with a few lines of code because of the capabilities of Leaflet
*/
    $.widget("wgm.imgNotes2", $.wgm.imgViewer2, {
        options: {
/*
*	Default action for addNote callback
*/
            addNote: function(data) {
                var icon = L.divIcon({
                    className: getCircleClassByStatus(data.Status),
                    html: "<i></i>",
                    iconSize: [20, 20],
                    iconAnchor: [10, 10]
                });
                var map = this.map,
                    loc = this.imgPosToLatLng(data.x, data.y),
                    popup = L.responsivePopup({ autoPanPadding: [10,10],  }).setContent('<div class="spinner-border" role="status"><span class="sr-only"></span></div>');
                var marker = L.marker(loc, {
                    icon: icon
                }).bindTooltip(
                    getTooltipTextByStatus(data.Status),
                    {
                        direction: "top",
                        className: 'e-tooltip'
                        //offset: L.point(0,-10)
                    }
                ).bindPopup(popup, {className: 'e-popup'})
                .addTo(map).on('click', function(e){
                    // close tooltip
                    var selfTooltip = e.target.getTooltip();
                    map.closeTooltip(selfTooltip);
                    // go to marker
                    //map.panTo(loc);
                    var selfPopup = e.target.getPopup();

                    // Load popup
                    getPopupData(data.HouseId, function(data){
                        selfPopup.setContent(data);
                        selfPopup.update();
                    });
                    
                });
            },
            addIndication: function(data) {
                var self = this;
                var icon = L.divIcon({
                    className: 'e-type',
                    html: 
                        "<div class='e-type-label'><div class='icon' style='background-image: url("+
                        data.Source + ")'></div><div class='text'>" + data.Text + "</div></div>",
                    iconSize: [40, 40],
                    iconAnchor: [20, 20],
                    autoPanPadding: [20,40],
                    style: 'color: red'
                });
                var map = this.map,
                    loc = this.imgPosToLatLng(data.x, data.y),
                    popup = L.popup({offset: {x: 0, y: 0}, autoPanPadding: [20,40] });
                var marker = L.marker(loc, {
                    icon: icon
                }).bindPopup(popup, {className: 'e-popup-indicator'})
                .addTo(map).on('popupopen', function(e){
                    $(e.target._icon).find('.text').addClass('open');
                    $(e.target._icon).css('z-index', '1000');
                });
                marker.getPopup().on('remove', function(e){
                    $(e.target._source._icon).find('.text').removeClass('open');
                    $(e.target._source._icon).css('z-index', '999');
                });
            }
        },
/*
*	Add notes from a javascript array
*/
        import: function(notes) {
            if (this.ready) {
                var self = this;
                $.each(notes, function() {
                    self.options.addNote.call(self, this);
                });	
            }
        },
        importIndications: function(indications) {
            if (this.ready) {
                var self = this;
                $.each(indications, function() {
                    self.options.addIndication.call(self, this);
                });	
            }
        }
    });
    $(document).ready( function() {
        var $img = $("#mapImage").imgNotes2({
            // zoom step
            zoomStep: 0.5,

            // the limit on the maximum zoom level of the image
            zoomMax: 5,

            // is zoomable
            zoomable: true,

            // is draggable
            dragable: true,
            // callbacks
            onClick: function( e, pos ) {
                var self = this;
                if(mapPosEnabled)
                {
                    var imgpos = self.relposToImage(pos);
                    updateMapPosInput("'x' => "+imgpos.x+", 'y' => "+imgpos.y);
                }
            },
            onReady: function() {
                var self = this; 
                getHouses(function(notes){
                    self.import(notes);
                    if(mapPosEnabled)
                        $('.e-status').css('pointer-events', 'none');
                });
                getIndications(function(indications){
                    self.importIndications(indications);
                    if(mapPosEnabled)
                        $('.e-type').css('pointer-events', 'none');
                });
                if(mapPosEnabled)
                    $(self.map._container).css('cursor', 'default');
                
                var template = 
                    "<div class='e-village-map-ref'>" +
                        "<div class='e-status-ref'><div class='free'></div>"+$.mapTooltipLabels[1]+"</div>"+
                        "<div class='e-status-ref'><div class='bought'></div>"+$.mapTooltipLabels[2]+"</div>"+
                        "<div class='e-status-ref'><div class='reserved'></div>"+$.mapTooltipLabels[3]+"</div>"+
                    "</div>";
                $(self.map._container).find('.leaflet-control-container').append($(template));
            }
        });
    });
})(jQuery);