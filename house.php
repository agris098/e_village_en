<?php require '_global.php';?>
<!doctype html>
<html lang="en">
	<head>
		<title>House</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Bootstrap CSS -->
        <!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
        <link rel="stylesheet" href="/css/house.css">
        <link rel="stylesheet" href="/lightBox/css/lightbox.css">
		<!--END CSS -->
		<!--START JS -->
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->      
	</head>
	<body>
        <?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
        <?php include 'shared/_termometerWindowForm.php';?>
        <?php include 'shared/_contactWindowForm.php';?>
        <?php include 'shared/_modalThanks.php';?>
		<div class="container">
            <h2 class="my-5 e-text-anime">House XL</h2>
            <div class="row row-eq-height">
                <div class="col-lg-8">
                    <div class="e-slider-mini-wrapper">
                        <div id="mini-slider" class="carousel slide e-slider e-slider-mini carousel-fade" data-ride="carousel">
                            <!-- The slideshow -->
                            <div class="carousel-inner">                               
                                <div class="carousel-item active" style="background-image: url(/img/jpg/house/xl_1.jpg);">
                                    <a class="lightbox-img" href="/img/jpg/house/xl_1.jpg" data-lightbox="house"></a>
                                </div>
                                <div class="carousel-item" style="background-image: url(/img/jpg/house/xl_2.png);">
                                    <a class="lightbox-img" href="/img/jpg/house/xl_2.png" data-lightbox="house"></a>
                                </div>
                                <div class="carousel-item" style="background-image: url(/img/jpg/house/xl_3.png);">
                                    <a class="lightbox-img" href="/img/jpg/house/xl_3.png" data-lightbox="house"></a>
                                </div>
                                <div class="carousel-item" style="background-image: url(/img/jpg/house/xl_4.png);">
                                    <a class="lightbox-img" href="/img/jpg/house/xl_4.png" data-lightbox="house"></a>
                                </div>
                                <div class="carousel-item" style="background-image: url(/img/jpg/house/xl_5.png);">
                                    <a class="lightbox-img" href="/img/jpg/house/xl_5.png" data-lightbox="house"></a>
                                </div>
                            </div>
                            <!-- Left and right controls -->
                            <div class="slider-controls d-flex align-items-center justify-content-center">
                                <a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider" data-slide="prev">
                                    <img src="img/svg/arrow_slider_left.svg">
                                </a>
                                <span class="slide-counter">1 / 6</span>
                                <a class="h-100 align-items-center justify-content-center d-flex" href="#mini-slider" data-slide="next">
                                    <img src="img/svg/arrow_slider_right.svg">
                                </a>
                            </div>
                            <div class="special-order">
                                Special order
                            </div>
                        </div>
                    </div>
                    <div class="e-image-anime right e-bg-white"></div>
                </div>
                <div class="col-lg-4 table-wrapper mt-4 mt-lg-0 e-text-anime">
                    <table class="table table-striped table-borderless e-detail-table">
                        <tr>
                            <td>Area:</td>
                            <td>142,48 м2</td>
                        </tr>
                        <tr>
                            <td>Storeys:</td>
                            <td>2</td>
                        </tr> 
                        <tr>
                            <td>Dimensions:</td>
                            <td>7,5m х 10,35m</td>
                        </tr>
                        <tr>
                            <td>Bedrooms:</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="price-row">
                                House Price:<br />
                                <strong class="price red"> 999,999 Dagcoins</strong>
                                <!-- <p class="usualprice">Common price: 2,000,000 Dagcoins</p> -->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="price-row">
                                Land Price: <br />
                                <strong class="price red">1,400 Euro</strong>
                                <!-- <p class="usualprice">Common price: 1,400 Euro</p> -->
                            </td>
                        </tr>               
                    </table>
                    <button class="e-btn e-btn-white w-100 e-contact-window-open">Leave request</button>
                </div>
            </div>
        </div>
        <div class="container e-mtb-100">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="e-text-anime mb-5">Architectural project</h2>
                </div>
                <div class="col-md-8 e-text-anime">
                    A cozy two-storey house of laminated veneer lumber that adopt fachwerk technology with a total area of ​​142 sq. m. The area of ​​terraces and balconies is 45 sq.m. This house is very convinient for a family of upto 4people. The first floor is occupied by a common family zone - a spacious combined living-dining room, a small kitchen, and a bathroom. The living area is located on the second floor - there is space for three bedrooms and a bathroom. From the rooms of the second floor there is convinient access to two balconies.
                </div>
            </div>
        </div>
        <!-- <div class="container e-mt-100 e-mb-180">
            <div class="row">
                <div class="col-md-4">
                    <h2 class="e-panel-anime mb-5">Equipment</h2>
                </div>
                <div class="col-md-8">
                    <div class="panel-group e-panel e-panel-black" id="rmapPanel">
                        <div class="panel e-panel-anime">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem1">
                            <h4 class="panel-title">
                                Освещение улиц
                            </h4>
                            </div>
                            <div id="rmapItem1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Высокое качество.
                                </div>
                            </div>
                        </div>
                        <div class="panel e-panel-anime">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem2">
                            <h4 class="panel-title">
                                Освещение улиц
                            </h4>
                            </div>
                            <div id="rmapItem2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Высокое качество.
                                </div>
                            </div>
                        </div>
                        <div class="panel e-panel-anime">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem3">
                            <h4 class="panel-title">
                                Освещение улиц
                            </h4>
                            </div>
                            <div id="rmapItem3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Высокое качество.
                                </div>
                            </div>
                        </div>
                    </div>												
                </div>
            </div>
        </div> -->
        <!-- START JS -->
        <script src="/js/global.js"></script>
        <script src="/js/termometer.js"></script>
        <script src="/lightBox/js/lightbox.js"></script>
        <!-- END JS -->
  </body>
</html>