<?php
    header('Content-type:application/json;charset=utf-8');
    $data = [
        (object) [ 'Price' => '27,000 €','Count' => '86-95 зем. участки'],
        (object) [ 'Price' => '25,000 €','Count' => '76-85 зем. участки'],
        (object) [ 'Price' => '23,000 €','Count' => '66-75 зем. участки'],
        (object) [ 'Price' => '21,000 €','Count' => '56-65 зем. участки'],
        (object) [ 'Price' => '19,500 €','Count' => '46-55 зем. участки'],
        (object) [ 'Price' => '18,000 €','Count' => '36-45 зем. участки'],
        (object) [ 'Price' => '16,500 €','Count' => '26-35 зем. участки', 'Active' => true],
        (object) [ 'Price' => '15,000 €','Count' => '1-25 зем. участки']
    ];
    echo json_encode($data);
?>