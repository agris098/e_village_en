<?php
    header('Content-type:application/json;charset=utf-8');
    $data = [
        (object) ['HouseId' => 1,'x' => 3342,'y' => 251, 'Status' => 1],
        (object) ['HouseId' => 2,'x' => 1860,'y' => 669, 'Status' => 2],
        (object) ['HouseId' => 3,'x' => 2110,'y' => 1317, 'Status' => 1],
        (object) ['HouseId' => 4,'x' => 2890,'y' => 1063, 'Status' => 3],
        (object) ['HouseId' => 5,'x' => 736,'y' => 1538, 'Status' => 3],
        (object) ['HouseId' => 6,'x' => 2337,'y' => 970, 'Status' => 2],
        (object) ['HouseId' => 7,'x' => 1631,'y' => 871, 'Status' => 1],
        (object) ['HouseId' => 8,'x' => 1476,'y' => 1098, 'Status' => 2],
        (object) ['HouseId' => 9,'x' => 3811,'y' => 587, 'Status' => 3],
        (object) ['HouseId' => 10,'x' => 1591,'y' => 1271, 'Status' => 1]
    ];
    echo json_encode($data);
?>