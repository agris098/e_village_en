<?php
    header('Content-type:application/json;charset=utf-8');
    $data = [
        (object) ['IndicationId' => 1,'x' => 3100,'y' => 400, 'Source' => '/img/svg/temp.png', 'Text' => 'ONE'],
        (object) ['IndicationId' => 2,'x' => 2000,'y' => 300, 'Source' => '/img/svg/temp.png', 'Text' => 'TWO'],
        (object) ['IndicationId' => 3,'x' => 2000,'y' => 1000, 'Source' => '/img/svg/temp.png', 'Text' => 'THREE'],
        (object) ['IndicationId' => 4,'x' => 2500,'y' => 1063, 'Source' => '/img/svg/temp.png', 'Text' => 'FOUR'],
        (object) ['IndicationId' => 5,'x' => 300,'y' => 1200, 'Source' => '/img/svg/temp.png', 'Text' => 'FIFE']
    ];
    echo json_encode($data);
?>