<?php
    header('Content-type:application/json;charset=utf-8');
    $data = [
        (object) [ 'Year' => 2019,'Month' => 2],
        (object) [ 'Year' => 2019,'Month' => 4],
        (object) [ 'Year' => 2019,'Month' => 5],
        (object) [ 'Year' => 2019,'Month' => 6],
        (object) [ 'Year' => 2019,'Month' => 7],
        (object) [ 'Year' => 2019,'Month' => 8],
        (object) [ 'Year' => 2019,'Month' => 9],
        (object) [ 'Year' => 2020,'Month' => 3],
        (object) [ 'Year' => 2020,'Month' => 4]
    ];
    echo json_encode($data);
?>