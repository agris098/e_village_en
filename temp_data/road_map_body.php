<?php
    header('Content-type:application/json;charset=utf-8');
    $data = [];
    $y = $_GET['Year'];
    $m = $_GET['Month'];

    if($y == 2019 && $m == 2)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Feb 12',
                'Title' => 'Search and selection of land for the construction of the village',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 4)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Apr 13',
                'Title' => 'Conclusion of a land purchase agreement',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Apr 10',
                'Title' => 'Style selection',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Apr 5',
                'Title' => 'Choosing a house manufacturer',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 5)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 May 22',
                'Title' => 'The choice of an architect',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 May 11',
                'Title' => 'Announcement of the project to the Onecoin crypto community (we are waiting for the opening of the exchanger)',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 6)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Jun 25',
                'Title' => 'Dagcoin crypto community project announcement',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Jun 14',
                'Title' => 'Announcement to Lyoness crypto community',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 7)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Jul 25',
                'Title' => 'Registration as a merchant in crypto communities',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Jul 14',
                'Title' => 'Consultation',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Jul 1',
                'Title' => 'Website creation',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 8)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Aug 18',
                'Title' => 'TK from the Municipality for sewage',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2019 && $m == 9)
    {
        array_push($data,
            (object) [ 
                'Date' => '2019 Sep 20',
                'Title' => 'Ground Check',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Sep 19',
                'Title' => 'Getting architectural - planning tasks for design',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Sep 18',
                'Title' => 'TK from Latvenergo',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2019 Sep 17',
                'Title' => 'TK from Latvijas Gase',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2020 && $m == 3)
    {
        array_push($data,
            (object) [ 
                'Date' => '2020 Mar 21',
                'Title' => 'General Contractor',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2020 Mar 15',
                'Title' => 'Getting architectural - planning tasks for design',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2020 Mar 10',
                'Title' => 'TK from Latvenergo',
                'Body' => 'Soon...'
            ]
        );
    }
    else if($y == 2020 && $m == 4)
    {
        array_push($data,
            (object) [ 
                'Date' => '2020 Apr 9',
                'Title' => 'Land surveying',
                'Body' => 'Soon...'
            ],
            (object) [ 
                'Date' => '2020 Apr 5',
                'Title' => 'Obtaining a building permit',
                'Body' => 'Soon...'
            ]
        );
    }
    echo json_encode($data);
?>