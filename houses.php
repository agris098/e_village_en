<?php require '_global.php';?>
<!doctype html>
<html lang="en">
	<head>
		<title>House projects</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
		<link rel="stylesheet" href="/css/houses.css">
        <!--END CSS -->
        <!--START JS -->
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->
	</head>
	<body>
        <?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
        <?php include 'shared/_termometerWindowForm.php';?>
        <?php include 'shared/_contactWindowForm.php';?>
		<div class="container">
           <h2 class="mt-5 mb-3 e-text-anime">House projects</h2>
           <div class="row e-mb-180">
                <div class="col-md-6 col-xl-4 py-4">
                    <div style="cursor: pointer;" class="card h-100 e-shadow e-text-anime">
                        <div class="e-card-header" style="background-image: url('/img/jpg/houses/1.jpg');">
                            <div class="e-card-header-area">142 м2</div>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">House XL</h3>
                            <p class="text-muted">Two-storey house using fachwerk technology</p>
                        </div>
                        <div class="card-footer">
                            <a href="/house" class="e-card-button">View</a>
                        </div>
                        <div class="e-card-special">Special Deal</div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 py-4">
                    <div style="cursor: pointer;" class="card h-100 e-shadow e-text-anime">
                        <div class="e-card-header" style="background-image: url('/img/jpg/houses/2.jpg');">
                            <div class="e-card-header-area">200 м2</div>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">House XXL</h3>
                            <p class="text-muted">Two-storey house using fachwerk technology</p>
                        </div>
                        <div class="card-footer">
                            <a href="/house1" class="e-card-button">View</a>
                        </div>
                        <div class="e-card-special">Special Deal</div>
                    </div>
                </div>
<!--                 <div class="col-md-6 col-xl-4 py-4">
                    <div style="cursor: pointer;" class="card h-100 e-shadow e-text-anime">
                        <div class="e-card-header" style="background-image: url('/img/jpg/houses/3.jpg');">
                            <div class="e-card-header-area">213,6 м2</div>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">House XXXL</h3>
                            <p class="text-muted">Двухэтажный дом по технологии «фахверк»</p>
                        </div>
                        <div class="card-footer">
                            <a href="/house1" class="e-card-button">View</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <script src="/js/global.js"></script>
        <script src="/js/termometer.js"></script>
        <script src="/js/houses.js"></script>
  </body>
</html>