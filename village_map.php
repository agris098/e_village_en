<?php require '_global.php';?>
<!doctype html>
<html lang="ru">
  	<head>
		<title>Village Map</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/leaflet/leaflet.css" />
		<link rel="stylesheet" href="/leaflet/leaflet.responsive.popup.css" />
		<link rel="stylesheet" href="/leaflet/Control.FullScreen.css" />
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
		<link rel="stylesheet" href="/css/village_map.css">
		<!--END CSS -->
		<!--START JS -->		
 		<script src="/js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="/leaflet/leaflet.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/jquery-ui.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.waypoints.min.js"></script>
		<script src="/js/underscore.min.js"></script>		
		<script src="/leaflet/leaflet.responsive.popup.js"></script>	
		<script src="/leaflet/Control.FullScreen.js"></script>
		<script src="/js/imgViewer2.js"></script>
		<!--END JS -->
	</head>
	<body>
		<?php include 'shared/_header.php';?>
		<?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_contactWindowForm.php';?>
		<?php include 'shared/_modalThanks.php';?>
		<div class="e-village-map">
			<img  id="mapImage" src="/img/jpg/village_map.jpg" width="100%" height="100%" style="visibility: hidden;" />
		</div>
		</div>
		<script src="/js/global.js"></script>
		<script src="/js/termometer.js"></script>
		<script src="/js/village_map.js"></script>
		<script>
			// Override Tooltip Labels
			$.mapTooltipLabels[1] = 'Cвободен';
			$.mapTooltipLabels[2] = 'Продан';
			$.mapTooltipLabels[3] = 'Зарезервирон';
		</script>
  	</body>
</html>