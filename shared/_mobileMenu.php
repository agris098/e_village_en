<div id="e-mobile-menu" class="e-mobile-menu">
    <div class="header plr">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 d-flex align-items-center">
                    <span class="title"><a href="/">Home</a></span>
                    <div style="display: flex;flex: auto;"></div>
                    <button id="e-mobile-menu-close" class="btn btn-link pr-0">
                        <img src="/img/svg/ic_burger_close.svg">
                    </button>
                </div>
            </div>
        </div> 
    </div>
    <div class="content plr">
        <div class="container">
            <ul class="list-unstyled m-0">
                <li class="sub-menu lang">
                    <a class="lang-item" href="<?php echo $enRequestUri;?>">EN</a>
                    <a class="lang-item" href="<?php echo $ruRequestUri;?>">RU</a>
                </li>
                <li class="sub-menu"><a href="/village_map">Village Map</a></li>
                <li class="sub-menu"><a href="/houses">House projects</a></li>
                <li class="sub-menu"><a href="/price">Price and procedure</a></li>
                <li><a href="/road_map">Road map</a></li>
                <li><a class="e-termometer-window-open">Price termometer</a></li>
                <li><a href='/faq'>FAQ</a></li>
                <li><a href='/news'>News</a></li>
                <li><a href='/contact'>Contacts</a></li>
                <li class="contact">
                    <img src="/img/svg/ic_instagram.svg">
                    <img src="/img/svg/ic_facebook.svg">
                    <a href="https://www.youtube.com/channel/UCEnctkA1cRcqaXwGdHW7Few/featured" target="_blank"><img src="/img/svg/ic_youtube_white.svg"></a>
                </li>
            </ul>
        </div>
    </div>
</div>