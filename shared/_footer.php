<footer class="e-footer">
    <div class="container-fluid e-bg-dark e-footer-projects">
        <div class="row e-bg-dark e-border-tb">
            <div class="container e-mtb-180">
                <div class="row">
                    <div class="col-lg-4">
                        <h2 class="mb-5 text-white e-text-anime">Our projects</h2>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-md-4 pb-5 pt-md-0 d-flex justify-content-center justify-content-md-start mb-md-0 mb-4">
                                <ul class="list-unstyled m-0 d-inline-block text-md-left text-center e-text-anime">
                                    <li><a class="e-btn e-btn-primary e-cursor-default">Active</a></li>
                                    <li class="e-hr"><img src="/img/svg/evillage_the_sea.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/dagcoin_ic.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/bitcoin_ic.svg"></li>
                                </ul>
                            </div>
                            <div class="col-md-4 py-5 py-md-0 d-flex justify-content-center justify-content-md-start mb-md-0 mb-4">
                                <ul class="list-unstyled m-0 d-inline-block text-md-left text-center e-text-anime">
                                    <li><a class="e-btn e-btn-disabled">Future project</a></li>
                                    <li class="e-hr"><img src="/img/svg/evillage_the_golf-1.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/dagcoin_grey.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/bitcoin_grey.svg"></li>
                                </ul>
                            </div>
                            <div class="col-md-4 pt-5 py-md-0 d-flex justify-content-center justify-content-md-start">
                                <ul class="list-unstyled m-0 d-inline-block text-md-left text-center e-text-anime">
                                    <li><a class="e-btn e-btn-disabled">Future project</a></li>
                                    <li class="e-hr"><img src="/img/svg/evillage_the_forest-1.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/dagcoin_grey.svg"></li>
                                    <li class="e-hr"><img src="/img/svg/bitcoin_grey.svg"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid e-bg-dark e-footer-menu">
        <div class="row e-bg-dark">
            <nav class="container d-block navbar navbar-expand-lg navbar-dark">
                <div class="row align-items-center" style="height: 100px;">
                    <div class="col-6 col-md-4">
                        © 2019  E-Village. 
                    </div>
                    <div class="col-6 col-md-8">
                        <div class="collapse navbar-collapse" id="collapsibleNavId">
                            <ul class="navbar-nav mt-2 mt-lg-0 w-100">
                                <li class="nav-item">
                                    <a class="nav-link" href="/road_map">Road map</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/news">News</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/faq">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/contact">Contacts</a>
                                </li>
                                <div style="display: flex;flex: auto;"></div>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><img src="/img/svg/ic_instagram.svg"></a>
                                </li>
                                <li class="nav-item"> 
                                    <a class="nav-link" href="#"><img src="/img/svg/ic_facebook.svg"></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link pr-0" target="_blank" href="https://www.youtube.com/channel/UCEnctkA1cRcqaXwGdHW7Few/featured"><img src="/img/svg/ic_youtube_white.svg"></a>
                                </li>
                            </ul>
                        </div>
                        <button class="e-mobile-menu-open navbar-toggler hidden-lg-up pull-right border-0 p-0" type="button">
                            <img class="hidden-lg-up pull-right" src="/img/svg/ic_burger_white.svg">
                        </button>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</footer>
<div class="e-mobile-menu-button e-mobile-menu-open">
</div>