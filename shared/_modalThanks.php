<div id="e-modal-thanks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="e-modal-thanks" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-link" data-dismiss="modal" aria-label="Close">
                    <img src="/img/svg/ic_burger_close_black.svg"/>
                </button>
            </div>
            <div class="modal-body py-0 pb-5 px-lg-5">
                <p style="font-size: 14px; color:#00B224;">Sent</p>
                <h2 class="my-5 py-4">Thanks for leaving request!</h2>
            </div>
        </div>
    </div>
</div>