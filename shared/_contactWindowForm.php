<div id="e-contact-window" class="e-contact-window e-bg-dark">
    <div class="content h-100">
        <div class="container h-100 d-flex">
            <button class="btn btn-link e-contact-window-close"><img src="/img/svg/ic_burger_close.svg"></button>
            <div class="w-100 m-auto" style="padding-top: 10px;padding-bottom: 10px;">
                <div class="row" style="padding-top: 90px; padding-bottom: 90px;">
                    <div class="col-lg-4">
                        <h2 class="mb-4 text-white">Fill out an request</h2>
                    </div>
                    <div class="col-lg-8">
                        <form action="#" method="POST" class="contact-form validate">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control form-control-lg" placeholder="Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control form-control-lg" placeholder="E-mail" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="country" class="form-control form-control-lg" placeholder="Country" required>
                            </div>
                            <div class="form-group">
                                <textarea type="" name="message" class="form-control form-control-lg" placeholder="Message" required></textarea>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="terms" class="custom-control-input" checked="checked" id="customCheck">
                                <label class="custom-control-label" for="customCheck">I give my consent to the processing of personal data</label>
                            </div>
                            <button class="mt-4 e-btn e-btn-light-long">SEND REQUEST</button>
                            <a class="mt-4 mb-3" href="/privacy_policy">Privacy policy</a>
                            <a href="/terms_of_use">Terms of Use</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>