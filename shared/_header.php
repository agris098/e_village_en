<header class="e-header w-100 e-bg-dark">
    <nav class="container navbar navbar-expand-xl">
        <div class="row h-100">
            <div class="col-4">
                <a class="navbar-brand" href="/"><img src="/img/svg/menu_logo.svg"></a>
            </div>
            <div class="col-8">
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mt-2 mt-lg-0 w-100">
                        <li class="nav-item">
                            <ul class="e-nav-list nav-link">
                                <li>
                                    <a href="/village_map">Village Map</a>
                                </li>
                                <li><a href="/houses">House projects</a></li>
                                <li><a href="/price">Price and procedure</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/road_map">Road map</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/news">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/faq">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact">Contacts</a>
                        </li>
                        <div style="display: flex;flex: auto;"></div>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EN</a>
                            <div class="dropdown-menu lang" aria-labelledby="dropdownId2">
                                <a class="dropdown-item" href="<?php echo $ruRequestUri;?>">RU</a>
                               <!--  <a class="dropdown-item" href="#">LV</a> -->
                            </div>
                        </li>
                    </ul>
                </div>
                <button class="e-mobile-menu-open navbar-toggler hidden-lg-up pull-right border-0 p-0" type="button">
                    <img class="hidden-lg-up pull-right" src="/img/svg/ic_burger_white.svg">
                </button>
            </div>
        </div>
    </nav>
</header>