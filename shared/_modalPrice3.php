<div id="e-modal-price-3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="e-modal-price-3" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-link" data-dismiss="modal" aria-label="Close">
                    <img src="/img/svg/ic_burger_close_black.svg"/>
                </button>
            </div>
            <div class="modal-body py-0 pb-lg-5 px-lg-5">
                <p style="font-size: 14px;"><img class="pr-3" style="padding-bottom: 4px;" src="/img/svg/ic_info.svg">Purchase terms</p>
                <div class="modal-body-scroll">
                    <h2>Program for EU Residents Permit</h2>
                    <p>
                        Comming soon...
                    </p>
                </div>
            </div>
            <div class="modal-footer px-lg-5 pb-lg-5">
                <a class="e-btn e-btn-primary text-center e-contact-window-open e-btn-lg">Reserve</a>
            </div>          
        </div>
    </div>
</div>