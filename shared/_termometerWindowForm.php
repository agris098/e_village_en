<div id="e-termometer-window" class="e-termometer-window e-bg-dark">
    <div class="content h-100">
        <div class="container h-100 d-flex">
            <button class="btn btn-link e-termometer-window-close"><img src="/img/svg/ic_burger_close.svg"></button>
            <div class="w-100 m-auto" style="padding-top: 10px;padding-bottom: 10px;">
                <!-- ------------- Content --------------------- -->
                <div class="e-termometer">
                    <div class="e-termometer-container">
                        <div style="display: flex; align-items: center;height: 100%;">
                            <div class="container">
                                <div class="row" style="padding-top: 90px; padding-bottom: 90px;">
                                    <div class="col-lg-5 left-c">
                                        <p class="e-termometer-table-header">number of reserved land plots</p>
                                        <p class="house-d">Land plots reserved</p>
                                        <p class="house-c">10</p>
                                    </div>
                                    <div class="col-lg-7 mt-5 mt-lg-0">
                                        <p class="e-termometer-table-header">price termometer</p>
                                        <div class="e-termometer-table-container">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ------------- Content --------------------- -->
            </div>
        </div>
    </div>
</div>