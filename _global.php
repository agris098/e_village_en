<?php
    $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
    $ruHost =  "ru.evillage.life";
    $enHost = "evillage.life";
    //$ruRequestUri = htmlspecialchars( "{$protocol}://{$ruHost}{$_SERVER['REQUEST_URI']}", ENT_QUOTES, 'UTF-8' );
    //$enRequestUri = htmlspecialchars( "{$protocol}://{$enHost}{$_SERVER['REQUEST_URI']}", ENT_QUOTES, 'UTF-8' ); 
    $ruRequestUri = htmlspecialchars( "http://{$ruHost}{$_SERVER['REQUEST_URI']}", ENT_QUOTES, 'UTF-8' );    
    $enRequestUri = htmlspecialchars( "https://{$enHost}{$_SERVER['REQUEST_URI']}", ENT_QUOTES, 'UTF-8' );         
?>