<?php require '_global.php';?>
<!doctype html>
<html lang="en">
  	<head>
		<title>Price and procedure</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
		<link rel="manifest" href="/favicon/site.webmanifest">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<!--START CSS -->
		<link rel="stylesheet" href="/css/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/css/global.css">
		<link rel="stylesheet" href="/css/animate.css">
		<!--END CSS -->
		<!--START JS -->
		<script src="/js/jquery-3.4.1.min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/popper.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.waypoints.min.js"></script>
		<!--END JS -->
	</head>
	<body>
		<?php include 'shared/_mobileMenu.php';?>
		<?php include 'shared/_header.php';?>
        <?php include 'shared/_termometerWindowForm.php';?>
        <?php include 'shared/_contactWindowForm.php';?>
		<div class="container e-price-container mt-5">
			<div class="row mb-5">
				<div class="col-12">
					<h2 class="e-text-anime">Price</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 mb-lg-0 mb-5 px-xl-4 e-text-anime">
					<div class="card e-border mb-3 e-shadow e-card h-100 ">
						<img class="e-card-icon e-modal-price-open-1" src="/img/svg/ic_info.svg">
						<div class="card-header e-card-header border-0 e-card-title">
							Without EU resident visa
						</div>
						<div class="card-body">
							<div class="e-card-border pt-3">
								<h2>15,000 €</h2>
								<p>Reservation fee</p>
							</div>
							<p class="pt-3">up to</p>
							<span class="e-card-title">2,000,000 Dagcoins</span>
						</div>
						<div class="e-card-footer p-0 border-0">
							<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-1">More</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-lg-0 mb-5 px-xl-4 e-text-anime">
					<div class="card e-border mb-3 e-card h-100 e-shadow">
						<img class="e-card-icon e-modal-price-open-2" src="/img/svg/ic_info.svg">
						<div class="card-header e-card-header border-0 e-card-title">
							With EU residents visa
						</div>
						<div class="card-body">
							<div class="e-card-border pt-3">
								<h2>75,000 €</h2>
								<p>Reservation fee</p>
							</div>
							<p class="pt-3">до</p>
							<span class="e-card-title">1,750,000 Dagcoins</span>
						</div>
						<div class="e-card-footer p-0 border-0">
							<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-2">More</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-lg-0 px-xl-4 e-text-anime">
					<div class="card mb-3 e-shadow e-border e-card h-100">
						<img class="e-card-icon e-modal-price-open-3" src="/img/svg/ic_info.svg">
						<div class="card-header e-card-header border-0 e-card-title">
							Program for EU Residents Permit
						</div>
						<div class="card-body">
							<div class="e-card-border pt-3">
								<h2>75,000 €</h2>
								<p>Investment</p>
							</div>
							<p class="pt-3">
								For those who not involved with crypto business, but has interest receive 5 years Residents Visa in EU for itself and members of family our project can create proper  solution.
							</p>
						</div>
						<div class="e-card-footer p-0 border-0">
							<a class="e-btn e-btn-primary w-100 text-center e-modal-price-open-3">More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container e-mt-100 e-mb-180">
			<div class="row">
				<div class="col-lg-4">
					<h2 class="e-panel-anime mb-5">Procedure</h2>
				</div>
				<div class="col-lg-8">
					<div class="panel-group e-panel e-panel-black" id="rmapPanel">
						<div class="panel e-panel-anime">
							<div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem1">
							<h4 class="panel-title">
								Без вида на жительство
							</h4>
							</div>
							<div id="rmapItem1" class="panel-collapse collapse in">
								<div class="panel-body">
									Высокое качество.
								</div>
							</div>
						</div>
						<div class="panel e-panel-anime">
							<div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem2">
							<h4 class="panel-title">
								C видом на жительство
							</h4>
							</div>
							<div id="rmapItem2" class="panel-collapse collapse in">
								<div class="panel-body">
									Высокое качество.
								</div>
							</div>
						</div>
						<div class="panel e-panel-anime">
							<div class="panel-heading" data-toggle="collapse" data-parent="#rmapPanel" href="#rmapItem3">
							<h4 class="panel-title">
								Инвесторам
							</h4>
							</div>
							<div id="rmapItem3" class="panel-collapse collapse in">
								<div class="panel-body">
									Высокое качество.
								</div>
							</div>
						</div>
					</div>												
				</div>
			</div>
		</div>
		<?php include 'shared/_modalPrice1.php';?>
		<?php include 'shared/_modalPrice2.php';?>
		<?php include 'shared/_modalPrice3.php';?>
		<?php include 'shared/_modalThanks.php';?>
		<script src="/js/global.js"></script>
		<script src="/js/termometer.js"></script>
  	</body>
</html>