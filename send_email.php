<?php
    //header('Content-type:application/json;charset=utf-8');
    // Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;

    if(isset($_POST['name']) && isset($_POST['email'])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $country = isset($_POST['country']) ? $_POST['country'] : "" ;
        $message = $_POST['message'];        

        // Load Composer's autoloader
        require_once 'PHPMailer/PHPMailer.php';
        require_once 'PHPMailer/SMTP.php';
        require_once 'PHPMailer/Exception.php';       
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
                                           
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->SMTPAutoTLS = false; 
            $mail->Host       = 'mail.evillage.life';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'reports.evillage';                     // SMTP username
            $mail->Password   = 'EvilReporterTool1!';                               // SMTP password
            //$mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                              // TCP port to connect to

            //Recipients
            $mail->AddReplyTo($email);
            $mail->setFrom('reports@evillage.life');
            $mail->addAddress("info@evillage.life");     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Evillage request';
            $mail->Body    = "<b>Name:</b> {$name}<br /><b>Country:</b> {$country}<br /> <b>Message:</b><br/>{$message}";
            $mail->AltBody = 'Name: {$name} Country: {$country} Message: {$message}';

            $mail->send();
            $response = 'Message has been sent';
            $status = 'OK';
        } catch (Exception $e) {
            $response = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            $status = 'FAIL';
        }
    } 
    else
    {
        $status = 'FAIL';
        $response = 'Name or Email not set!';
    }
    exit(json_encode(array('message' => $response, 'status' => $status)))
?>